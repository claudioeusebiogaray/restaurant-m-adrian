﻿using mph_restaurant.Model.Dynamo;
using mph_restaurant.Util.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace mph_restaurant.Service
{
    public class PublicationCRUD
    {
        private const string apiURL = "https://mje93fj34j.execute-api.us-east-1.amazonaws.com/dev/";
        private HttpClient httpClient;

        public async Task<Publication> GetLastPublication(string RestId, string Date)
        {
            httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Add("RestId", RestId);
            httpClient.DefaultRequestHeaders.Add("PubDate", Date);

            HttpResponseMessage response = await httpClient.GetAsync(apiURL + "publicationgetlast");

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine("responseString: " + responseString);

            Publication result = JsonConvert.DeserializeObject<Publication>(responseString);
            return result;
        }
        
        public async Task<PublicationResult> PostPublication(Publication publication)
        {
            httpClient = new HttpClient();

            string content = JsonConvert.SerializeObject(publication);

            HttpResponseMessage response = await httpClient.PostAsync(apiURL + "publicationcrud", new StringContent(content, Encoding.UTF8, "application/json"));

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine("responseString: " + responseString);

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);

            dict.TryGetValue("code", out string codeValue);

            switch (codeValue)
            {
                case ("201"):
                    return PublicationResult.Created;
                case ("400"):
                    return PublicationResult.ErrorWhenCreating;
                case ("409"):
                    return PublicationResult.APublicationUnderReview;
                default:
                    return PublicationResult.Unknown;
            }
        }
        
        public async Task<PublicationResult> DeletePublication(string publicationId)
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(apiURL)
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, "publicationcrud")
            {
                Content = new StringContent("{'PublicationId' : '" + publicationId + "'}", Encoding.UTF8, "application/json")
            };

            HttpResponseMessage response = await client.SendAsync(request);

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine("responseString: " + responseString);
            
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);

            dict.TryGetValue("code", out string codeValue);

            switch (codeValue)
            {
                case ("201"):
                    return PublicationResult.Removed;
                case ("400"):
                    return PublicationResult.ErrorDeleting;
                default:
                    return PublicationResult.Unknown;
            }
        }
    }
}
