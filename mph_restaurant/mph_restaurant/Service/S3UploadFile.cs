﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using mph_restaurant.Util.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace mph_restaurant.Service
{
    public class S3UploadFile
    {
        private static readonly RegionEndpoint BucketRegion  = RegionEndpoint.USEast1;
        private const string AccessKey = "AKIAJZ6WXFWCJ5YZITYQ";
        private const string SecretAccessKey  = "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";

        private const string bucketName = "menuparahoybucket";
        private IAmazonS3 s3Client;


        public async Task<S3UploadFileResult> UploadFile(string path, string key)
        {
            s3Client = new AmazonS3Client(AccessKey, SecretAccessKey, BucketRegion);
            try
            {
                var fileTransferUtility = new TransferUtility(s3Client);

                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = bucketName,
                    FilePath = path,
                    StorageClass = S3StorageClass.Standard,
                    PartSize = 6291456, // 6 MB.
                    CannedACL = S3CannedACL.PublicRead
                };

                if (!string.IsNullOrWhiteSpace(key))
                {
                    fileTransferUtilityRequest.Key = key;
                }

                await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
                
                return S3UploadFileResult.Complete;
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error S3UploadFile -> UploadFile -> AmazonS3Exception:", e);
                return S3UploadFileResult.AmazonS3Exception;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error S3UploadFile -> UploadFile:", e);
            }
            return S3UploadFileResult.Unknown;
        }
    }
}
