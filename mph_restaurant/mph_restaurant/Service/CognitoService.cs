﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using Amazon.Runtime;
using mph_restaurant.Interface;
using mph_restaurant.Util.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace mph_restaurant.Service
{
    public class CognitoService : ICognitoService
    {
        #region private Variables
        private const string ClientId = "41jgfbgre4k8n27betr8jhkdaq";
        private const string PoolId = "us-east-1_3VKfy7ujj";
        private readonly RegionEndpoint RegionEndpoint = RegionEndpoint.USEast1;
        #endregion

        #region public Variables
        public AmazonCognitoIdentityProviderClient AWSCognitoProvider { get; set; }
        public CognitoUserPool UserPool { get; set; }
        #endregion

        public CognitoService()
        {
            AWSCognitoProvider = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), RegionEndpoint);
            UserPool = new CognitoUserPool(PoolId, ClientId, AWSCognitoProvider);
        }

        public async Task<LogInContext> Login(string userName, string password)
        {
            try
            {
                CognitoUser user = new CognitoUser(userName, ClientId, UserPool, AWSCognitoProvider);

                AuthFlowResponse context = await user.StartWithSrpAuthAsync(new InitiateSrpAuthRequest()
                {
                    Password = password
                }).ConfigureAwait(false);

                if (context.ChallengeName == ChallengeNameType.NEW_PASSWORD_REQUIRED)
                    return new LogInContext()
                    {
                        Result = CognitoResult.NewPasswordRequired
                    };
                else
                    return new LogInContext()
                    {
                        Result = CognitoResult.Logined,
                        ExpirationTime = user.SessionTokens.ExpirationTime
                    };
            }
            catch (PasswordResetRequiredException e)
            {
                Console.WriteLine("Error PasswordResetRequired: " + e.Message);
                return new LogInContext()
                {
                    Result = CognitoResult.PasswordResetRequired
                };
            }
            catch (NotAuthorizedException e)
            {
                Console.WriteLine("Error NotAuthorized: " + e.Message);
                return new LogInContext()
                {
                    Result = CognitoResult.NotAuthorized
                };
            }
            catch (UserNotFoundException e)
            {
                Console.WriteLine("Error UserNotFound: " + e.Message);
                return new LogInContext()
                {
                    Result = CognitoResult.UserNotFound
                };
            }
            catch (UserNotConfirmedException e)
            {
                Console.WriteLine("Error UserNotConfirmed: " + e.Message);
                return new LogInContext()
                {
                    Result = CognitoResult.UserNotConfirmed
                };
            }
            catch (Exception e)
            {
                Console.WriteLine("Error CognitoService -> Login: " + e);
            }
            return new LogInContext()
            {
                Result = CognitoResult.Unknown
            };
        }
               
        public async Task<CognitoResult> SignUp(string userName, string password, string name, string phone)
        {
            try
            {
                SignUpRequest signUpRequest = new SignUpRequest()
                {
                    ClientId = ClientId,
                    Username = userName,
                    Password = password
                };

                AttributeType emailAttribute = new AttributeType()
                {
                    Name = "email",
                    Value = userName
                };
                AttributeType nameAttribute = new AttributeType()
                {
                    Name = "name",
                    Value = name
                };
                AttributeType phoneAttribute = new AttributeType()
                {
                    Name = "phone_number",
                    Value = phone
                };

                signUpRequest.UserAttributes.Add(emailAttribute);
                signUpRequest.UserAttributes.Add(nameAttribute);
                signUpRequest.UserAttributes.Add(phoneAttribute);

                var signUpResponse = await AWSCognitoProvider.SignUpAsync(signUpRequest);
                
                return CognitoResult.Registered;
            }
            catch (UsernameExistsException e)
            {
                Console.WriteLine("Error UsernameExists: " + e.Message);
                return CognitoResult.UsernameExists;
            }
            catch(InvalidPasswordException e)
            {
                Console.WriteLine("Error UsernameExists: " + e.Message);
                return CognitoResult.InvalidPassword;
            }
            catch (InvalidParameterException e)
            {
                Console.WriteLine("Error InvalidParameter: " + e.Message);
                return CognitoResult.InvalidParameter;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error CognitoService -> SignUp: " + e);
            }
            return CognitoResult.Unknown;
        }

        public async Task<CognitoResult> VerifyWithCode(string userName, string code)
        {
            try
            {
                var result = await AWSCognitoProvider.ConfirmSignUpAsync(new ConfirmSignUpRequest
                {
                    ClientId = ClientId,
                    Username = userName,
                    ConfirmationCode = code
                });

                return CognitoResult.Verified;
            }
            catch(CodeMismatchException e)
            {
                Console.WriteLine("Error CodeMismatchException: " + e.Message);
                return CognitoResult.CodeMismatch;
            }
            catch (InvalidLambdaResponseException e)
            {
                Console.WriteLine("Error InvalidLambdaResponseException: " + e.Message);
                return CognitoResult.InvalidLambdaResponse;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error CognitoService -> VerifyWithCode: " + e);
            }
            return CognitoResult.Unknown;
        }
    }
}
