﻿using mph_restaurant.Model.Dynamo;
using mph_restaurant.Util.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace mph_restaurant.Service
{
    public class RestaurantCRUD
    {
        private const string apiURL = "https://mje93fj34j.execute-api.us-east-1.amazonaws.com/dev/";
        private HttpClient httpClient;
        
        public async Task<Restaurant> GetRestaurant(string RestId)
        {
            httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Add("Id", RestId);

            HttpResponseMessage response = await httpClient.GetAsync(apiURL + "restaurantcrud");

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine("responseString: " + responseString);

            Restaurant result = JsonConvert.DeserializeObject<Restaurant>(responseString);
            return result;
        }

        public async Task<RestaurantResult> UpdateRestaurant (Restaurant restaurant)
        {
            httpClient = new HttpClient
            {
                BaseAddress = new Uri(apiURL)
            };
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string contentRestaurant = JsonConvert.SerializeObject(restaurant);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, "restaurantcrud")
            {
                Content = new StringContent(contentRestaurant, Encoding.UTF8, "application/json")
            };

            HttpResponseMessage response = await httpClient.SendAsync(request);

            var responseString = await response.Content.ReadAsStringAsync();

            Console.WriteLine("responseString: " + responseString);

            return RestaurantResult.Unknown;

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);

            dict.TryGetValue("code", out string codeValue);

            switch (codeValue)
            {
                case ("201"):
                    return RestaurantResult.Updated;
                case ("400"):
                    return RestaurantResult.ErrorUpdating;
                default:
                    return RestaurantResult.Unknown;
            }
        }
    }
}
