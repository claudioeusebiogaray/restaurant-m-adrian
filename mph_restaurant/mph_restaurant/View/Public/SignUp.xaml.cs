﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Public
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUp : ContentPage
    {
        private static SignUp instance = null;
        public static SignUp Instance
        {
            get
            {
                if (instance == null)
                    instance = new SignUp();

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        public SignUp()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}