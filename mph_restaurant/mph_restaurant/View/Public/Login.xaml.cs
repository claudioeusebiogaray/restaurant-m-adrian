﻿using mph_restaurant.Navigator;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Public
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        private static Login instance = null;
        public static Login Instance {
            get {
                if (instance == null)
                    instance = new Login();

                return instance;
            }
            set 
            {
                instance = value;
            }
        }

        public Login()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}