﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Public
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ValidateAccount : ContentPage
    {
        private static ValidateAccount instance = null;
        public static ValidateAccount Instance
        {
            get
            {
                if (instance == null)
                    instance = new ValidateAccount();

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        public ValidateAccount()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}