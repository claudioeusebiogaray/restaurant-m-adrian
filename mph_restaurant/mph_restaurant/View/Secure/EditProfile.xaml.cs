﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Secure
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProfile : ContentPage
    {
        private static EditProfile instance = null;
        public static EditProfile Instance
        {
            get
            {
                if (instance == null)
                    instance = new EditProfile();

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        public EditProfile()
        {
            InitializeComponent();
        }

        private async void Take_Photo_Clicked(object sender, EventArgs e)
        {
            try
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);

                    if (results.ContainsKey(Permission.Camera))
                        cameraStatus = results[Permission.Camera];
                }

                if (storageStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);

                    if (results.ContainsKey(Permission.Storage))
                        storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        SaveToAlbum = true,
                        AllowCropping = true,
                        DefaultCamera = CameraDevice.Rear,
                        Name = DateTime.Now.ToString(),
                        PhotoSize = PhotoSize.Full
                    });

                    if (file == null)
                        return;


                    PhotoSource.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        return stream;
                    });
                    
                    PhotoSourceText.Text = file.Path;
                }
                else if (cameraStatus != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Cámara negada", "No se puede continuar, prueba nuevamente.", "OK");
                }
                else if (storageStatus != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Almacenamiento negado", "No se puede continuar, prueba nuevamente.", "OK");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error EditProfile -> Take_Photo_Clicked: " + ex);
            }
        }

        private async void Gallery_Clicked(object sender, EventArgs e)
        {
            try
            {
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (storageStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);

                    if (results.ContainsKey(Permission.Storage))
                        storageStatus = results[Permission.Storage];
                }

                if (storageStatus == PermissionStatus.Granted)
                {
                    var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        PhotoSize = PhotoSize.Full
                    });

                    if (file == null)
                        return;


                    PhotoSource.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        return stream;
                    });
                    
                    PhotoSourceText.Text = file.Path;
                }
                else if (storageStatus != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Almacenamiento negado", "No se puede continuar, prueba nuevamente.", "OK");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error EditProfile -> Gallery_Clicked: " + ex);
            }
        }
    }
}