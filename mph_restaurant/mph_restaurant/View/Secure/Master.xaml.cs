﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Secure
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Master : ContentPage
    {
        private static Master instance = null;
        public static Master Instance
        {
            get
            {
                if (instance == null)
                    instance = new Master();

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        public Master()
        {
            InitializeComponent();
        }
    }
}