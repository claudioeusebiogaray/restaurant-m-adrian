﻿using mph_restaurant.View_Model.Secure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Secure
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailMain : MasterDetailPage
    {
        private static MasterDetailMain instance = null;
        public static MasterDetailMain Instance
        {
            get
            {
                if (instance == null)
                    instance = new MasterDetailMain();

                return instance;
            }
            set
            {
                instance = value;
            }
        }
        public MasterDetailMain()
        {
            InitializeComponent();
        }
        /*
            var master = Secure.Master.Instance;
            master.BindingContext = Master_VM;

            var home = Home.Instance;
            home.BindingContext = Home_VM;

            Master = master;
            Detail = new NavigationPage(home);
         */
    }
}