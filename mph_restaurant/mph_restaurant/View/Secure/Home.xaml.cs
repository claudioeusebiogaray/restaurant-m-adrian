﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Secure
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : ContentPage
    {
        private static Home instance = null;
        public static Home Instance
        {
            get
            {
                if (instance == null)
                    instance = new Home();

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        public Home()
        {
            InitializeComponent();
        }
    }
}