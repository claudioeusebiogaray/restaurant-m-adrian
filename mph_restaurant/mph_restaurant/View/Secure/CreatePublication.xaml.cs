﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace mph_restaurant.View.Secure
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreatePublication : ContentPage
    {
        private static CreatePublication instance = null;
        public static CreatePublication Instance
        {
            get
            {
                if (instance == null)
                    instance = new CreatePublication();

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        public CreatePublication()
        {
            InitializeComponent();
        }

        private async void Take_Photo_Clicked(object sender, EventArgs e)
        {
            try
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);

                    if (results.ContainsKey(Permission.Camera))
                        cameraStatus = results[Permission.Camera];
                }

                if (storageStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);

                    if (results.ContainsKey(Permission.Storage))
                        storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        SaveToAlbum = true,
                        AllowCropping = true,
                        DefaultCamera = CameraDevice.Rear,
                        Name = DateTime.Now.ToString(),
                        PhotoSize = PhotoSize.Full
                    });

                    if (file == null)
                        return;


                    PhotoSource.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        return stream;
                    });
                    
                    PhotoSourceText.Text = file.Path;
                }
                else if (cameraStatus != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Cámara negada", "No se puede continuar, prueba nuevamente.", "OK");
                }
                else if (storageStatus != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Almacenamiento negado", "No se puede continuar, prueba nuevamente.", "OK");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error CreatePublication -> Take_Photo_Clicked: " + ex);
            }
        }

        private async void Gallery_Clicked(object sender, EventArgs e)
        {
            try
            {
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (storageStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);

                    if (results.ContainsKey(Permission.Storage))
                        storageStatus = results[Permission.Storage];
                }

                if (storageStatus == PermissionStatus.Granted)
                {
                    var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        PhotoSize = PhotoSize.Medium
                    });

                    if (file == null)
                        return;


                    PhotoSource.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        return stream;
                    });
                    
                    PhotoSourceText.Text = file.Path;
                }
                else if (storageStatus != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Almacenamiento negado", "No se puede continuar, prueba nuevamente.", "OK");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error CreatePublication -> Gallery_Clicked: " + ex);
            }
        }

        private async void TimePicker_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Time")
            {
                try
                {
                    int min = 11, max = 14;
                    bool message = false;

                    switch (Type.SelectedItem.ToString())
                    {
                        case "Almuerzo":
                            min = 11;
                            max = 14;
                            break;
                        case "Desayuno":
                            min = 7;
                            max = 10;
                            break;
                        case "Cena":
                            min = 19;
                            max = 22;
                            break;
                    }


                    if (Start.Time.Hours < min)
                    {
                        Start.Time = new TimeSpan(min, Start.Time.Minutes, 0);
                        message = true;
                    }
                    else if (Start.Time.Hours > max)
                    {
                        Start.Time = new TimeSpan(max, Start.Time.Minutes, 0);
                        message = true;
                    }

                    if (Ending.Time.Hours < min)
                    {
                        Ending.Time = new TimeSpan(min, Ending.Time.Minutes, 0);
                        message = true;
                    }
                    else if (Ending.Time.Hours > max)
                    {
                        Ending.Time = new TimeSpan(max, Ending.Time.Minutes, 0);
                        message = true;
                    }

                    if (message)
                        await DisplayAlert("Range time", "The chosen time range is not in the allowed range. Has been corrected automatically.", "Ok");
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error CreatePublication -> TimePicker_PropertyChanged: " + ex);
                }
            }
        }

        private void Type_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedItem")
            {
                try
                {
                    int min = 11, max = 14;
                    switch (Type.SelectedItem?.ToString())
                    {
                        case "Almuerzo":
                            min = 11;
                            max = 14;
                            break;
                        case "Desayuno":
                            min = 7;
                            max = 10;
                            break;
                        case "Cena":
                            min = 19;
                            max = 22;
                            break;
                    }
                    if (Start.Time.Hours < min)
                        Start.Time = new TimeSpan(min, Start.Time.Minutes, 0);
                    else if (Start.Time.Hours > Ending.Time.Hours)
                    {
                        Start.Time = new TimeSpan(Ending.Time.Hours, 0, 0);
                        Ending.Time = new TimeSpan(Ending.Time.Hours, 45, 0);
                    }

                    if (Ending.Time.Hours < Start.Time.Hours)
                    {
                        Start.Time = new TimeSpan(Start.Time.Hours, 0, 0);
                        Ending.Time = new TimeSpan(Start.Time.Hours, 45, 0);
                    }
                    else if (Ending.Time.Hours > max)
                        Ending.Time = new TimeSpan(max, Ending.Time.Minutes, 0);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error CreatePublication -> Type_PropertyChanged: " + ex);
                }
            }
        }
    }
}