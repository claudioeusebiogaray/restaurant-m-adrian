﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using mph_restaurant.Interface;
using mph_restaurant.Service;
using mph_restaurant.Util.Enum;
using mph_restaurant.View.Secure;
using Xamarin.Forms;

namespace mph_restaurant.View_Model.Secure
{
    public class Master_VM : Base_VM
    {
        #region Binding Buttons
        public ICommand CmdCreatePublication { get; set; }
        public ICommand CmdProfile { get; set; }
        public ICommand CmdLogOut { get; set; }
        #endregion

        public Master_VM(INavigator<Event_Secure> iNavigator_Secure, ICognitoService iCognitoService, ISession iSession) : base(iNavigator_Secure, iCognitoService, iSession)
        {
            CmdCreatePublication = new Command(ToCreatePublication);
            CmdProfile = new Command(ToEditProfile);
            CmdLogOut = new Command(DoLogOut);
        }
        protected virtual async void ToCreatePublication()
        {
            await Task.Run(async () =>
            {
                IsBusy = true;
                try
                {
                    IsBusy = false;
                    Session.Publication = await new PublicationCRUD().GetLastPublication(Session.Username, DateTime.Now.ToString("dd/MM/yyyy"));
                    MasterDetailMain.Instance.IsPresented = false;
                    await Navigator_Secure.OnNavigationEvent(Event_Secure.ToCreatePublication);

                    return;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error Master_VM -> ToCreatePublication: " + e);
                }
                IsBusy = false;
            });
        }

        protected virtual async void ToEditProfile()
        {
            MasterDetailMain.Instance.IsPresented = false;
            await Navigator_Secure.OnNavigationEvent(Event_Secure.ToEditProfile);
        }

        protected virtual async void DoLogOut()
        {
            await Task.Run(async () =>
            {
                IsBusy = true;
                try
                {
                    Session.Logout();

                    IsBusy = false;
                    MasterDetailMain.Instance.IsPresented = false;
                    await ToLogIn();

                    return;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error Master_VM -> DoLogOut: " + e);
                }
                IsBusy = false;
            });
        }

        protected virtual async Task ToLogIn()
        {
            await Navigator_Secure.OnNavigationEvent(Event_Secure.ToLogin);
        }
    }
}
