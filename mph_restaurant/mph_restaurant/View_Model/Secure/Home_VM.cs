﻿using System;
using System.Collections.Generic;
using System.Text;
using mph_restaurant.Interface;
using mph_restaurant.Model.Dynamo;
using mph_restaurant.Util.Enum;

namespace mph_restaurant.View_Model.Secure
{
    public class Home_VM : Base_VM
    {
        public Restaurant Restaurant { get; set; }
        public Home_VM(INavigator<Event_Secure> iNavigator_Secure, ICognitoService iCognitoService, ISession iSession) : base(iNavigator_Secure, iCognitoService, iSession)
        {
            Restaurant = iSession.Restaurant;
        }
    }
}
