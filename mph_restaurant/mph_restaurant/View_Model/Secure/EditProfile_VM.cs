﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using mph_restaurant.Interface;
using mph_restaurant.Model.Dynamo;
using mph_restaurant.Model.Secure;
using mph_restaurant.Service;
using mph_restaurant.Util.Enum;
using Xamarin.Forms;

namespace mph_restaurant.View_Model.Secure
{
    class EditProfile_VM : Base_VM
    {
        public Restaurant Restaurant { get; set; }
        public EditProfile_M EditProfile_M { get; set; }

        public ICommand CmdUpdateProfile { get; set; }

        public EditProfile_VM(INavigator<Event_Secure> iNavigator_Secure, ICognitoService iCognitoService, ISession iSession) : base(iNavigator_Secure, iCognitoService, iSession)
        {
            Restaurant = iSession.Restaurant;
            EditProfile_M = new EditProfile_M(Restaurant);

            CmdUpdateProfile = new Command(DoUpdateProfile);
        }

        protected virtual async void DoUpdateProfile()
        {
            await Task.Run(async () =>
            {
                IsBusy = true;

                try
                {
                    if (!string.IsNullOrWhiteSpace(EditProfile_M.ImagePath))
                    {
                        var logoName = Session.Username;
                        logoName = logoName.Replace(" ", "").Replace(".", "").Replace(",", "").Replace(":", "").Replace(";", "").Replace("/", "").Replace("@", "");

                        Restaurant.LogoUrl = "https://s3.amazonaws.com/menuparahoybucket/" + logoName + ".jpg";
                        
                        IsBusy = true;
                        var resultS3 = await new S3UploadFile().UploadFile(EditProfile_M.ImagePath, logoName + ".jpg");
                        IsBusy = false;
                        switch (resultS3)
                        {
                            case S3UploadFileResult.AmazonS3Exception:
                            case S3UploadFileResult.Unknown:
                                await Navigator_Secure.OnAlertEvent(Event_Secure.AmazonS3Exception);
                                return;
                        }
                    }
                                       
                    Restaurant.Services = EditProfile_M.Services();
                    IsBusy = true;
                    var result = await new RestaurantCRUD().UpdateRestaurant(Restaurant);
                    IsBusy = false;
                    switch (result)
                    {
                        case RestaurantResult.Updated:
                            await Navigator_Secure.OnAlertEvent(Event_Secure.Updated);
                            break;
                        case RestaurantResult.ErrorUpdating:
                            await Navigator_Secure.OnAlertEvent(Event_Secure.ErrorUpdating);
                            break;
                        case RestaurantResult.Unknown:
                            await Navigator_Secure.OnAlertEvent(Event_Secure.Unknown);
                            return;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error EditProfile_VM -> DoUpdateProfile: " + e);
                }
                IsBusy = false;
            });
        }
    }
}
