﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using mph_restaurant.Interface;
using mph_restaurant.Model.Dynamo;
using mph_restaurant.Model.Secure;
using mph_restaurant.Service;
using mph_restaurant.Util.Enum;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace mph_restaurant.View_Model.Secure
{
    public class CreatePublication_VM : Base_VM
    {
        #region Binding Buttons
        public ICommand CmdPublicate { get; set; }
        #endregion

        public Restaurant Restaurant { get; set; }

        public bool isPremium = false;
        public bool IsPremium
        {
            get
            {
                return isPremium;
            }
            set
            {
                isPremium = value;
                Device.BeginInvokeOnMainThread(() =>
                {
                    NotifyPropertyChanged(nameof(IsPremium));
                    NotifyPropertyChanged(nameof(IsNotPremium));
                });
            }
        }
        public bool IsNotPremium
        {
            get { return !isPremium; }
        }
        public CreatePublication_M CreatePublication_M { get; set; }
        public CreatePublication_VM(INavigator<Event_Secure> iNavigator_Secure, ICognitoService iCognitoService, ISession iSession) : base(iNavigator_Secure, iCognitoService, iSession)
        {
            Restaurant = iSession.Restaurant;
            IsPremium = Restaurant.Premium;

            if (iSession.Publication != null)
            {
                CreatePublication_M = new CreatePublication_M(iSession.Publication.Type)
                {
                    MinPrice = iSession.Publication.MinPrice,
                    MaxPrice = iSession.Publication.MaxPrice,
                };

                var start = iSession.Publication.StartTime.Split(':');
                var ending = iSession.Publication.EndingTime.Split(':');
                CreatePublication_M.StartTime = new TimeSpan(int.Parse(start[0]), int.Parse(start[1]), int.Parse(start[2]));
                CreatePublication_M.EndingTime = new TimeSpan(int.Parse(ending[0]), int.Parse(ending[1]), int.Parse(ending[2]));
            }
            else
                CreatePublication_M = new CreatePublication_M("");

            CmdPublicate = new Command(DoPublicate);
        }

        protected virtual async void DoPublicate()
        {
            await Task.Run(async () =>
            {
                IsBusy = true;
                try
                {
                    if (!string.IsNullOrWhiteSpace(CreatePublication_M.ImagePath))
                    {
                        var pubId = Session.Username + DateTime.Now.ToString("HHmmddMMyyyy");
                        pubId = pubId.Replace(" ", "").Replace(".", "").Replace(",", "").Replace(":", "").Replace(";", "").Replace("/", "").Replace("@", "");

                        var publication = new Publication()
                        {
                            PublicationId = pubId,
                            RestaurantId = Session.Username,
                            Type = CreatePublication_M.TypePublication.Name,
                            StartTime = CreatePublication_M.StartTime.ToString(),
                            EndingTime = CreatePublication_M.EndingTime.ToString(),
                            MinPrice = CreatePublication_M.MinPrice,
                            MaxPrice = CreatePublication_M.MaxPrice,
                            ImageUrl = "https://s3.amazonaws.com/menuparahoybucket/" + pubId + ".jpg",
                            Description = CreatePublication_M.Description,
                            Date = DateTime.Now.ToString("dd/MM/yyyy"),
                            Status = "0",

                            Time = DateTime.Now.ToString("HH:mm"),
                        };

                        var result = await new PublicationCRUD().PostPublication(publication);

                        IsBusy = false;
                        switch (result)
                        {
                            case PublicationResult.Created:
                                IsBusy = true;
                                var resultS3 = await new S3UploadFile().UploadFile(CreatePublication_M.ImagePath, pubId + ".jpg");
                                IsBusy = false;
                                switch (resultS3)
                                {
                                    case S3UploadFileResult.Complete:
                                        await Navigator_Secure.OnAlertEvent(Event_Secure.Created);
                                        break;
                                    case S3UploadFileResult.AmazonS3Exception:
                                    case S3UploadFileResult.Unknown:
                                        await new PublicationCRUD().DeletePublication(publication.PublicationId);
                                        await Navigator_Secure.OnAlertEvent(Event_Secure.AmazonS3Exception);
                                        break;
                                }
                                break;
                            case PublicationResult.ErrorWhenCreating:
                                await Navigator_Secure.OnAlertEvent(Event_Secure.ErrorCreating);
                                break;
                            case PublicationResult.APublicationUnderReview:
                                await Navigator_Secure.OnAlertEvent(Event_Secure.APublicationUnderReview);
                                break;
                            case PublicationResult.Unknown:
                                await Navigator_Secure.OnAlertEvent(Event_Secure.Unknown);
                                break;
                        }
                    }
                    else
                    {
                        await Navigator_Secure.OnAlertEvent(Event_Secure.SelectAnImage);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error CreatePublication_VM -> DoPublicate: " + e);
                }
                IsBusy = false;
            });
        }
    }
}
