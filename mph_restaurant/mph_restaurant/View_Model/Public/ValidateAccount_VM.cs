﻿using mph_restaurant.Interface;
using mph_restaurant.Model.Util;
using mph_restaurant.Util.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace mph_restaurant.View_Model.Public
{
    public class ValidateAccount_VM : Base_VM
    {

        #region Binding Buttons
        public ICommand CmdValidate { get; set; }
        #endregion

        public Cognito_M ValidateAccount_M { get; set; }
        public ValidateAccount_VM(INavigator<Event_Public> iNavigator_Public, ICognitoService iCognitoService, ISession iSession) : base(iNavigator_Public, iCognitoService, iSession)
        {
            ValidateAccount_M = new Cognito_M
            {
                Email = iSession.Username
            };
            CmdValidate = new Command(DoValidate);
        }

        protected virtual async void DoValidate()
        {
            await Task.Run(async () =>
            {
                IsBusy = true;
                try
                {
                    if (string.IsNullOrWhiteSpace(ValidateAccount_M.Email) || string.IsNullOrWhiteSpace(ValidateAccount_M.ConfirmationCode))
                    {
                        IsBusy = false;
                        return;
                    }

                    var user = ValidateAccount_M.Email?.Trim();
                    var code = ValidateAccount_M.ConfirmationCode?.Trim();

                    var result = await CognitoService.VerifyWithCode(user, code);

                    IsBusy = false;
                    switch (result)
                    {
                        case CognitoResult.Verified:
                            await Navigator_Public.OnAlertEvent(Event_Public.Verified);
                            break;
                        case CognitoResult.CodeMismatch:
                            await Navigator_Public.OnAlertEvent(Event_Public.CodeMismatch);
                            break;
                        case CognitoResult.Unknown:
                            await Navigator_Public.OnAlertEvent(Event_Public.Unknown);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error ValidateAccount_VM -> DoValidate: " + e);
                }
                IsBusy = false;
            });
        }
    }
}
