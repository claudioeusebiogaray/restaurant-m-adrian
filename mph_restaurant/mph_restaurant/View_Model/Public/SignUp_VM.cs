﻿using mph_restaurant.Interface;
using mph_restaurant.Model.Util;
using mph_restaurant.Util.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace mph_restaurant.View_Model.Public
{
    class SignUp_VM : Base_VM
    {
        #region Binding Buttons
        public ICommand CmdSignUp { get; set; }
        #endregion

        public Cognito_M SignUp_M { get; set; }

        private bool termsAccepted = false;
        public bool TermsAccepted
        {
            get
            {
                return termsAccepted;
            }
            set
            {
                termsAccepted = value;
                Device.BeginInvokeOnMainThread(() =>
                {
                    NotifyPropertyChanged(nameof(TermsAccepted));
                });
            }
        }

        public SignUp_VM(INavigator<Event_Public> iNavigator_Public, ICognitoService iCognitoService, ISession iSession) : base(iNavigator_Public, iCognitoService, iSession)
        {
            SignUp_M = new Cognito_M();
            CmdSignUp = new Command(DoSignUp);
        }

        protected virtual async void DoSignUp()
        {
            await Task.Run(async () =>
            {
                IsBusy = true;
                try
                {
                    if (string.IsNullOrWhiteSpace(SignUp_M.Email) || string.IsNullOrWhiteSpace(SignUp_M.Password)
                        || string.IsNullOrWhiteSpace(SignUp_M.ConfirmPassword) || string.IsNullOrWhiteSpace(SignUp_M.Name)
                        || string.IsNullOrWhiteSpace(SignUp_M.Mobile))
                    {
                        IsBusy = false;
                        await Navigator_Public.OnAlertEvent(Event_Public.AllFieldsAreRequiered);
                        return;
                    }

                    if (SignUp_M.Password != SignUp_M.ConfirmPassword)
                    {
                        IsBusy = false;
                        await Navigator_Public.OnAlertEvent(Event_Public.PasswordDontMatch);
                        return;
                    }

                    var user = SignUp_M.Email?.Trim();
                    var pass = SignUp_M.Password?.Trim();
                    var name = SignUp_M.Name?.Trim().ToUpper();
                    var phon = SignUp_M.Mobile?.Trim();
                    
                    var result = await CognitoService.SignUp(user, pass, name, phon);

                    IsBusy = false;

                    switch (result)
                    {
                        case CognitoResult.Registered:
                            await Navigator_Public.OnAlertEvent(Event_Public.Registered);
                            break;
                        case CognitoResult.InvalidPassword:
                            await Navigator_Public.OnAlertEvent(Event_Public.InvalidPassword);
                            break;
                        case CognitoResult.InvalidParameter:
                            await Navigator_Public.OnAlertEvent(Event_Public.InvalidParameter);
                            break;
                        case CognitoResult.UsernameExists:
                            await Navigator_Public.OnAlertEvent(Event_Public.UsernameExists);
                            break;
                        case CognitoResult.Unknown:
                            await Navigator_Public.OnAlertEvent(Event_Public.Unknown);
                            break;
                    }
                    return;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error SignUp_VM -> DoSignUp: " + e);
                }
                IsBusy = false;
            });
        }
    }
}
