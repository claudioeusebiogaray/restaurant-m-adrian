﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using mph_restaurant.Interface;
using mph_restaurant.Model.Util;
using mph_restaurant.Service;
using mph_restaurant.Util.Enum;
using Xamarin.Forms;

namespace mph_restaurant.View_Model.Public
{
    public class Login_VM : Base_VM
    {
        #region Binding Buttons
        public ICommand CmdLogin { get; set; }
        public ICommand CmdSignUp { get; set; }
        public ICommand CmdForgotPassword { get; set; }
        #endregion

        public Cognito_M Login_M { get; set; }
        public Login_VM(INavigator<Event_Public> iNavigator_Public, ICognitoService iCognitoService, ISession iSession) : base(iNavigator_Public, iCognitoService, iSession)
        {
            Login_M = new Cognito_M();
            CmdLogin = new Command(DoLogin);
            CmdSignUp = new Command(ToSignUp);
        }

        protected virtual async void ToSignUp()
        {
            await Navigator_Public.OnNavigationEvent(Event_Public.ToSignUp);
        }

        protected virtual async void DoLogin()
        {
            await Task.Run(async () =>
            {
                IsBusy = true;
                try
                {
                    if (string.IsNullOrWhiteSpace(Login_M.Email) || string.IsNullOrWhiteSpace(Login_M.Password))
                    {
                        IsBusy = false;
                        return;
                    }

                    var user = Login_M.Email?.Trim();
                    var pass = Login_M.Password?.Trim();
                    
                    var result = await CognitoService.Login(user, pass);
                    IsBusy = false;

                    switch (result.Result)
                    {
                        case CognitoResult.Logined:

                            IsBusy = true;
                            var restaurant = await new RestaurantCRUD().GetRestaurant(user);

                            Session.Restaurant = restaurant;
                            Session.Username = user;
                            Session.TokenExpirationTime = result.ExpirationTime;
                            
                            IsBusy = false;

                            await Navigator_Public.OnNavigationEvent(Event_Public.ToHome);
                            break;
                        case CognitoResult.UserNotFound:
                            await Navigator_Public.OnAlertEvent(Event_Public.UserNotFound);
                            break;
                        case CognitoResult.NotAuthorized:
                            await Navigator_Public.OnAlertEvent(Event_Public.NotAuthorized);
                            break;
                        case CognitoResult.UserNotConfirmed:
                            Session.Username = user;
                            await Navigator_Public.OnAlertEvent(Event_Public.UserNotConfirmed);
                            break;
                        case CognitoResult.Unknown:
                            await Navigator_Public.OnAlertEvent(Event_Public.Unknown);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error Login_VM -> DoLogin: " + e);
                }
                IsBusy = false;
            });
        }
    }
}
