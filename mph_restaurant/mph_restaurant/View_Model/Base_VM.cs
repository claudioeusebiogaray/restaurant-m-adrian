﻿using mph_restaurant.Interface;
using mph_restaurant.Util.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace mph_restaurant.View_Model
{
    public class Base_VM : INotifyPropertyChanged
    {
        #region Property Changed Stuff
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        private bool isbusy = false;
        public bool IsBusy
        {
            get { return isbusy; }
            set
            {
                isbusy = value;
                Device.BeginInvokeOnMainThread(() =>
                {
                    NotifyPropertyChanged(nameof(IsNotBusy));
                    NotifyPropertyChanged(nameof(IsBusy));
                });
            }
        }
        public bool IsNotBusy
        {
            get { return !isbusy; }
        }

        public INavigator<Event_Public> Navigator_Public { get; set; }
        public INavigator<Event_Secure> Navigator_Secure { get; set; }
        public ICognitoService CognitoService { get; set; }
        public ISession Session { get; set; }

        public Base_VM(INavigator<Event_Public> iNavigator_Public, ICognitoService iCognitoService, ISession iSession)
        {
            Navigator_Public = iNavigator_Public;
            CognitoService = iCognitoService;
            Session = iSession;
        }

        public Base_VM(INavigator<Event_Secure> iNavigator_Secure, ICognitoService iCognitoService, ISession iSession)
        {
            Navigator_Secure = iNavigator_Secure;
            CognitoService = iCognitoService;
            Session = iSession;
        }
    }
}
