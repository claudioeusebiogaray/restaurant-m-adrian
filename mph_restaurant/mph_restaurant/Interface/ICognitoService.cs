﻿using mph_restaurant.Util.Enum;
using System;
using System.Threading.Tasks;

namespace mph_restaurant.Interface
{
    public class LogInContext
    {
        public CognitoResult Result { get; set; }
        public LogInContext(CognitoResult res = CognitoResult.Unknown)
        {
            Result = res;
        }
        public DateTime ExpirationTime { get; set; }
    }
    public interface ICognitoService
    {
        Task<LogInContext> Login(string userName, string password);
        Task<CognitoResult> SignUp(string userName, string password, string name, string phone);
        Task<CognitoResult> VerifyWithCode(string userName, string code);
    }
}
