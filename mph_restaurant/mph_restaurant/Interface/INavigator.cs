﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace mph_restaurant.Interface
{
    public interface INavigator<EnumType>
    {
        ICognitoService CognitoService { get; set; }
        INavigation Navigation { get; set; }
        ISession Session { get; set; }
        Page Page { get; set; }
        Task OnNavigationEvent(EnumType event_Nav);
        Task OnAlertEvent(EnumType event_Nav);
    }
}
