﻿using mph_restaurant.Model.Dynamo;
using System;
using Plugin.Settings.Abstractions;

namespace mph_restaurant.Interface
{
    public interface ISession
    {
        ISettings Settings { get; set; }

        string Username { get; set; }
        DateTime TokenExpirationTime { get; set; }
        bool IsLoggedIn(DateTime now);
        void Logout();

        Restaurant Restaurant { get; set; }
        Publication Publication { get; set; }
    }
}
