﻿using mph_restaurant.Interface;
using mph_restaurant.Model.Util;
using mph_restaurant.Navigator;
using mph_restaurant.Service;
using mph_restaurant.Util;
using mph_restaurant.Util.Enum;
using mph_restaurant.View.Secure;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace mph_restaurant
{
    public partial class App : Application
    {
        public ICognitoService CognitoService { get; set; }
        public ISession Session { get; set; }

        public App()
        {
            InitializeComponent();

            CognitoService = new CognitoService();
            Session = new Session { Settings = Plugin.Settings.CrossSettings.Current };

            //Session.Logout();
            InitializeMainPage();
        }


        public void InitializeMainPage(bool auth = false)
        {

            if (Session.IsLoggedIn(DateTime.Now))
                Authenticated();
            else
                Unauthenticated();
        }

        protected void Unauthenticated()
        {
            var navigator = new Navigator_Public
            {
                CognitoService = CognitoService,
                Session = Session
            };

            var page_M = Page_M.CreatePage_Public(PageId_Public.Login, navigator, navigator.CognitoService, Session);

            var navigationPage = new NavigationPage(page_M.Page);

            navigator.Page = navigationPage;
            navigator.Navigation = navigationPage.Navigation;

            MainPage = navigationPage;
        }

        protected void Authenticated()
        {
            var navigator = new Navigator_Secure
            {
                CognitoService = CognitoService,
                Session = Session
            };

            var page_M = Page_M.CreatePage_Secure(PageId_Secure.MasterDetail, navigator, navigator.CognitoService, Session);
            
            navigator.Page = page_M.Page;
            navigator.Navigation = MasterDetailMain.Instance.Detail.Navigation;

            MainPage = page_M.Page;
        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
