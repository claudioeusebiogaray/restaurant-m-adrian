﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mph_restaurant.Model.Util
{
    public class Cognito_M
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string ConfirmationCode { get; set; }
    }
}
