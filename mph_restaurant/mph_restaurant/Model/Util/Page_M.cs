﻿using mph_restaurant.Interface;
using mph_restaurant.Util.Enum;
using mph_restaurant.View.Public;
using mph_restaurant.View.Secure;
using mph_restaurant.View_Model;
using mph_restaurant.View_Model.Public;
using mph_restaurant.View_Model.Secure;
using Xamarin.Forms;

namespace mph_restaurant.Model.Util
{
    public class Page_M
    {
        public Page Page { get; set; }

        public Page_M(Page page)
        {
            Page = page;
        }

        public Page_M(Page page, Base_VM base_VM)
        {
            Page = page;
            Page.BindingContext = base_VM;
        }

        public static Page_M CreatePage_Public(PageId_Public id, INavigator<Event_Public> iNavigator, ICognitoService iCognitoService, ISession iSession)//, ISessionStore sessionStore)
        {
            Page_M page_M = null;

            switch (id)
            {
                case PageId_Public.Login:
                    page_M = new Page_M(Login.Instance, new Login_VM(iNavigator, iCognitoService, iSession));
                    break;
                case PageId_Public.SignUp:
                    page_M = new Page_M(SignUp.Instance, new SignUp_VM(iNavigator, iCognitoService, iSession));
                    break;
                case PageId_Public.ValidateAccount:
                    page_M = new Page_M(ValidateAccount.Instance, new ValidateAccount_VM(iNavigator, iCognitoService, iSession));
                    break;
            }

            return page_M;
        }

        public static Page_M CreatePage_Secure(PageId_Secure id, INavigator<Event_Secure> iNavigator, ICognitoService iCognitoService, ISession iSession)
        {
            Page_M page_M = null;

            switch (id)
            {
                case PageId_Secure.MasterDetail:
                    var masterDetailMain = MasterDetailMain.Instance;

                    masterDetailMain.Master = CreatePage_Secure(PageId_Secure.Master, iNavigator, iCognitoService, iSession).Page;
                    masterDetailMain.Detail = new NavigationPage(CreatePage_Secure(PageId_Secure.Home, iNavigator, iCognitoService, iSession).Page);
                    masterDetailMain.IsPresented = false;

                    page_M = new Page_M(masterDetailMain);
                    break;
                case PageId_Secure.Master:
                    page_M = new Page_M(Master.Instance, new Master_VM(iNavigator, iCognitoService, iSession));
                    break;
                case PageId_Secure.Home:
                    page_M = new Page_M(Home.Instance, new Home_VM(iNavigator,iCognitoService,iSession));
                    break;
                case PageId_Secure.CreatePublication:
                    page_M = new Page_M(CreatePublication.Instance, new CreatePublication_VM(iNavigator,iCognitoService,iSession));
                    break;
                case PageId_Secure.EditProfile:
                    page_M = new Page_M(EditProfile.Instance, new EditProfile_VM(iNavigator, iCognitoService, iSession));
                    break;
            }

            return page_M;
        }
    }
}
