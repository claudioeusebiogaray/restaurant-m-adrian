﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mph_restaurant.Model.Secure
{
    public class CreatePublication_M
    {
        public List<TypePublication> TypesPublications { get; set; } = new List<TypePublication>
            {
                new TypePublication() { Id = 0, Name = "Almuerzo" },
                new TypePublication() { Id = 1, Name = "Desayuno" },
                new TypePublication() { Id = 2, Name = "Cena" }
            };
        public TypePublication TypePublication { get; set; }
        public string MinPrice { get; set; }
        public string MaxPrice { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndingTime { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }

        public CreatePublication_M(string type)
        {
            TypesPublications = new List<TypePublication>
            {
                new TypePublication() { Id = 0, Name = "Almuerzo" },
                new TypePublication() { Id = 1, Name = "Desayuno" },
                new TypePublication() { Id = 2, Name = "Cena" }
            };
            switch (type)
            {
                case "Almuerzo":
                    TypePublication = TypesPublications[0];
                    break;
                case "Desayuno":
                    TypePublication = TypesPublications[1];
                    break;
                case "Cena":
                    TypePublication = TypesPublications[2];
                    break;
                default:
                    TypePublication = TypesPublications[0];
                    break;
            }
        }
    }

    public class TypePublication
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
