﻿using mph_restaurant.Model.Dynamo;
using System;
using System.Collections.Generic;
using System.Text;

namespace mph_restaurant.Model.Secure
{
    public class EditProfile_M
    {
        public bool A { get; set; }
        public bool B { get; set; }
        public bool C { get; set; }
        public bool D { get; set; }
        public bool E { get; set; }
        public bool F { get; set; }
        public bool G { get; set; }
        public string ImagePath { get; set; }

        public EditProfile_M(Restaurant restaurant)
        {
            if (restaurant != null)
            {
                if (restaurant.Services.TryGetValue("A", out bool DicA)) A = DicA;
                if (restaurant.Services.TryGetValue("B", out bool DicB)) B = DicB;
                if (restaurant.Services.TryGetValue("C", out bool DicC)) C = DicC;
                if (restaurant.Services.TryGetValue("D", out bool DicD)) D = DicD;
                if (restaurant.Services.TryGetValue("E", out bool DicE)) E = DicE;
                if (restaurant.Services.TryGetValue("F", out bool DicF)) F = DicF;
                if (restaurant.Services.TryGetValue("G", out bool DicG)) G = DicG;
            }
        }

        public Dictionary<string, bool> Services()
        {
            return new Dictionary<string, bool>
            {
                { "A", A },
                { "B", B },
                { "C", C },
                { "D", D },
                { "E", E },
                { "F", F },
                { "G", G },
            };
        }
    }
}
