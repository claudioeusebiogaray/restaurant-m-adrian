﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mph_restaurant.Model.Dynamo
{
    public class Publication
    {
        public string PublicationId { get; set; } = " ";
        ///<summary>Date of creation of publication</summary>
        public string Date { get; set; } = " ";
        public string Description { get; set; } = " ";
        public string EndingTime { get; set; } = " ";
        ///<summary>Url of image on scale small</summary>
        public string ImageUrlSmall { get; set; } = " ";
        ///<summary>Url of image on scale medium</summary>
        public string ImageUrlMedium { get; set; } = " ";
        ///<summary>Url of image on scale large</summary>
        public string ImageUrlLarge { get; set; } = " ";
        ///<summary>Url of image without scale</summary>
        public string ImageUrl { get; set; } = " ";
        public string MaxPrice { get; set; } = " ";
        public string MinPrice { get; set; } = " ";
        public string StartTime { get; set; } = " ";
        /// <summary>Status of the publication (int)
        /// <para>Code 0: New</para>
        /// <para>Code 1: Accepted</para>
        /// <para>Code 2: Rejected</para>
        /// <para>Code 3: Disabled</para>
        /// </summary>
        public string Status { get; set; } = "0";
        ///<summary>Time of creation of publication</summary>
        public string Time { get; set; } = " ";
        public string Type { get; set; } = "Almuerzo";

        public string RestaurantId { get; set; }
    }
}
