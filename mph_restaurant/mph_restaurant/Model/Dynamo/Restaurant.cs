﻿using System.Collections.Generic;

namespace mph_restaurant.Model.Dynamo
{
    public class Restaurant
    {
        public string RestaurantId { get; set; } = "";
        public string Address { get; set; } = "";
        public string Description { get; set; } = "";
        /// <summary>
        /// Latitude of restaurant address 
        /// </summary>
        public string Latitude { get; set; } = "";
        /// <summary>
        /// Length of restaurant address 
        /// </summary>
        public string Longitude { get; set; } = "";
        ///<summary>Url of Logo on scale small</summary>
        public string LogoUrlSmall { get; set; } = "";
        ///<summary>Url of Logo on scale medium</summary>
        public string LogoUrlMedium { get; set; } = "";
        ///<summary>Url of Logo on scale large</summary>
        public string LogoUrlLarge { get; set; } = "";
        ///<summary>Url of Logo without scale</summary>
        public string LogoUrl { get; set; } = "";
        public string MobileNumber { get; set; } = "";
        public string Name { get; set; } = "";
        public string PhoneNumber { get; set; } = "";
        /// <summary>Status of the Restaurant (int)
        /// <para>"A": Card</para>
        /// <para>"B": Parking</para>
        /// <para>"c": Air-conditioned</para>
        /// <para>"D": Smoking area</para>
        /// <para>"E": Cash</para>
        /// <para>"F": Wi-Fi</para>
        /// <para>"G": To take away</para>
        /// </summary>
        public Dictionary<string, bool> Services { get; set; } = new Dictionary<string, bool>(){
            { "A", false },
            { "B", false },
            { "C", false },
            { "D", false },
            { "E", false },
            { "F", false },
            { "G", false }
        };
        /// <summary>Status of the Restaurant (int)
        /// <para>0: New</para>
        /// <para>1: Accepted</para>
        /// <para>2: Rejected</para>
        /// <para>3: Disabled</para>
        /// </summary>
        public string Status { get; set; } = "0";
        public string Rating { get; set; } = "";
        public bool Premium { get; set; } = false;
    }
}
