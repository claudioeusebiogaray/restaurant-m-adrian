﻿using mph_restaurant.Interface;
using mph_restaurant.Model.Dynamo;
using System;
using Plugin.Settings.Abstractions;
using Newtonsoft.Json;

namespace mph_restaurant.Util
{
    public class Session : ISession
    {
        public ISettings Settings { get; set; }

        public string Username
        {
            get { return Settings.GetValueOrDefault(nameof(Username), string.Empty); }
            set { Settings.AddOrUpdateValue(nameof(Username), value); }
        }
        public DateTime TokenExpirationTime
        {
            get { return Settings.GetValueOrDefault(nameof(TokenExpirationTime), default(DateTime)); }
            set { Settings.AddOrUpdateValue(nameof(TokenExpirationTime), value); }
        }
        public bool IsLoggedIn(DateTime now)
        {
            return TokenExpirationTime > now;
        }
        public void Logout()
        {
            Username = string.Empty;
            TokenExpirationTime = default(DateTime);
        }

        public Restaurant Restaurant
        {
            get { return JsonConvert.DeserializeObject<Restaurant>(Settings.GetValueOrDefault(nameof(Restaurant), null)); }
            set { Settings.AddOrUpdateValue(nameof(Restaurant), JsonConvert.SerializeObject(value)); }
        }
        public Publication Publication
        {
            get { return JsonConvert.DeserializeObject<Publication>(Settings.GetValueOrDefault(nameof(Publication), null)); }
            set { Settings.AddOrUpdateValue(nameof(Publication), JsonConvert.SerializeObject(value)); }
        }

    }
}
