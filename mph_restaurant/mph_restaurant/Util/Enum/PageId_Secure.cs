﻿namespace mph_restaurant.Util.Enum
{
    public enum PageId_Secure
    {
        Home,
        EditProfile,
        CreatePublication,

        MasterDetail,
        Master,
    }
}
