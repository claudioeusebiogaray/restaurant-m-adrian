﻿namespace mph_restaurant.Util.Enum
{
    public enum PageId_Public
    {
        Login,
        SignUp,
        ValidateAccount,
        UpdatePassword
    }
}
