﻿namespace mph_restaurant.Util.Enum
{
    public enum PublicationResult
    {
        Created,
        APublicationUnderReview,
        ErrorWhenCreating,

        Removed,
        ErrorDeleting,
        Updated,
        Unknown
    }
}
