﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mph_restaurant.Util.Enum
{
    public enum RestaurantResult
    {
        Updated,
        ErrorUpdating,
        Unknown
    }
}
