﻿namespace mph_restaurant.Util.Enum
{
    public enum Event_Public
    {
        #region Navigation Event
        ToLogin,
        ToSignUp,
        ToValidateAccount,
        ToHome,
        #endregion

        #region Alert Event
        Registered,
        Verified,
        NotAuthorized,
        UserNotConfirmed,
        UserNotFound,
        NewPasswordRequired,
        PasswordResetRequired,
        UsernameExists,
        InvalidPassword,
        InvalidParameter,
        CodeMismatch,
        PasswordDontMatch,
        AllFieldsAreRequiered,
        #endregion

        Unknown,
    }
}
