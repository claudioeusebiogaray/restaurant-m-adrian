﻿namespace mph_restaurant.Util.Enum
{
    public enum S3UploadFileResult
    {
        Complete,
        AmazonS3Exception,
        Unknown,
    }
}
