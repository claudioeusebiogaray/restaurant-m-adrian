﻿namespace mph_restaurant.Util.Enum
{
    public enum Event_Secure
    {
        ToHome,
        ToEditProfile,
        ToCreatePublication,
        ToLogin,

        Created,
        ErrorCreating,
        APublicationUnderReview,
        AmazonS3Exception,
        SelectAnImage,

        Updated,
        ErrorUpdating,

        Unknown,
    }
}
