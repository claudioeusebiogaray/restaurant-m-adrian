﻿namespace mph_restaurant.Util.Enum
{
    public enum CognitoResult
    {
        Logined,
        Registered,
        Verified,
        NotAuthorized,
        UserNotConfirmed,
        UserNotFound,
        NewPasswordRequired,
        PasswordResetRequired,
        UsernameExists,
        InvalidPassword,
        InvalidParameter,
        CodeMismatch,
        InvalidLambdaResponse,
        Unknown,
    }
}
