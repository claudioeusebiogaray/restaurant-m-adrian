﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mph_restaurant.Util
{
    public class Strings_Public
    {
        public static string Login_Ok  = "";
        public static string Login_Fail = "Fallo al iniciar sesión";

        public static string AllFieldsAreRequiered = "Por favor complete todos los campos para poder registrarse.";
        public static string PasswordDontMatch = "Las contraseñas no coinciden, por favor vuelva a intentarlo.";
        public static string UserNotConfirmed = "El usuario no está validado.";
        public static string UserNotFound = "El usuario no existe.";
        public static string NotAuthorized = "Nombre de usuario o contraseña incorrecta.";
        public static string NewPasswordRequired = "Se requiere nueva la contraseña para el usuario. Póngase en contacto con el soporte técnico.";
        public static string PasswordResetRequired = "Se requiere restablecer la contraseña para el usuario. Póngase en contacto con el soporte técnico.";

        public static string SignUp_Ok  = "Registro Completado";
        public static string SignUp_Fail = "Registro Fallido";

        public static string Registered  = "Ha recibido un correo electronico con un codigo de validacion. Ingrese el codigo en la siguiente pantalla para completar el registro.";
        public static string UsernameExists  = "Ya existe una cuenta con el correo electrónico dado.";
        public static string InvalidPassword  = "La contraseña debe tener: \n- letras mayúsculas\n- letras minusculas\n- mínimo ocho caracteres\n- caracteres numéricos\n- caracteres simbólicos";
        public static string InvalidParameter  = "Formato de número de teléfono no válido.";

        public static string ValidateAccount_Ok = "Cuenta Verificada";
        public static string ValidateAccount_Fail = "Verificación fallida";

        public static string Verified  = "Su cuenta ha sido verificada correctamente. Ahora puede ingresar.";
        public static string CodeMismatch  = "Se proporcionó un código de verificación no válido, inténtalo de nuevo.";

        public static string Unknown_T = "Fallo desconocido";
        public static string Unknown_M = "Ha ocurrido un error desconocido, por favor pongase en contacto con soporte técnico.";
    }
}
