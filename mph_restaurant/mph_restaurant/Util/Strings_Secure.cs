﻿namespace mph_restaurant.Util
{
    public class Strings_Secure
    {
        public static string CreatePublication_Ok = "Publicación Creada";
        public static string CreatePublication_Fail = "Fallo al Crear Publicación";

        public static string CP_Created = "La publicación fue creada exitosamente.";
        public static string CP_ErrorCreating = "Ocurrio un error al crear la publicación.";
        public static string CP_APublicationUnderReview = "Tienes una publicación en revisión.";
        public static string CP_SelectAnImage = "Seleccione una imagen antes de enviar la publicación.";

        public static string AmazonS3_Fail = "Fallo al cargar el archivo";
        public static string AmazonS3Exception = "Error al cargar la image de la publicación.";
        
        public static string EditProfile_Ok = "Perfil Actualizado";
        public static string EditProfile_Fail = "Fallo al actualizar perfil";

        public static string EP_Updated = "Su perfil ha sido actualizado correctamente";
        public static string EP_ErrorUpdating = "Ocurrio un error al actualizar su perfil.";

        public static string Unknown_T = "Fallo desconocido";
        public static string Unknown_M = "Ha ocurrido un error desconocido, por favor pongase en contacto con soporte técnico.";
    }
}
