﻿using mph_restaurant.Interface;
using mph_restaurant.Model.Util;
using mph_restaurant.Util;
using mph_restaurant.Util.Enum;
using mph_restaurant.View.Secure;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace mph_restaurant.Navigator
{
    public class Navigator_Secure : INavigator<Event_Secure>
    {
        public ICognitoService CognitoService { get; set; }
        public INavigation Navigation { get; set; }
        public ISession Session { get; set; }
        public Page Page { get; set; }

        public virtual async Task OnAlertEvent(Event_Secure event_Secure)
        {
            string title = "Unknown";
            string message = "Unknown";
            string cancel = "Ok";

            switch (event_Secure)
            {
                case Event_Secure.Created:
                    title = Strings_Secure.CreatePublication_Ok;
                    message = Strings_Secure.CP_Created;
                    cancel = "Genial";
                    break;
                case Event_Secure.ErrorCreating:
                    title = Strings_Secure.CreatePublication_Fail;
                    message = Strings_Secure.CP_ErrorCreating;
                    break;
                case Event_Secure.APublicationUnderReview:
                    title = Strings_Secure.CreatePublication_Fail;
                    message = Strings_Secure.CP_APublicationUnderReview;
                    break;
                case Event_Secure.AmazonS3Exception:
                    title = Strings_Secure.AmazonS3_Fail;
                    message = Strings_Secure.AmazonS3Exception;
                    break;
                case Event_Secure.SelectAnImage:
                    title = Strings_Secure.CreatePublication_Fail;
                    message = Strings_Secure.CP_SelectAnImage;
                    break;
                case Event_Secure.Updated:
                    title = Strings_Secure.EditProfile_Ok;
                    message = Strings_Secure.EP_Updated;
                    cancel = "Genial";
                    break;
                case Event_Secure.ErrorUpdating:
                    title = Strings_Secure.EditProfile_Fail;
                    message = Strings_Secure.EP_ErrorUpdating;
                    break;
                case Event_Secure.Unknown:
                    title = Strings_Secure.Unknown_T;
                    message = Strings_Secure.Unknown_M;
                    break;
            }
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Page.DisplayAlert(title, message, cancel);

                switch (cancel)
                {
                    case "Genial":
                        await OnNavigationEvent(Event_Secure.ToHome);
                        CreatePublication.Instance = null;
                        EditProfile.Instance = null;
                        break;
                }
            });
        }

        public virtual async Task OnNavigationEvent(Event_Secure event_Secure)
        {
            Page_M page_M = null;

            switch (event_Secure)
            {
                case Event_Secure.ToLogin:
                    (Application.Current as App).InitializeMainPage();
                    break;
                case Event_Secure.ToHome:
                    await Navigation.PopToRootAsync();
                    break;
                case Event_Secure.ToEditProfile:
                    page_M = Page_M.CreatePage_Secure(PageId_Secure.EditProfile, this, CognitoService, Session);
                    break;
                case Event_Secure.ToCreatePublication:
                    page_M = Page_M.CreatePage_Secure(PageId_Secure.CreatePublication, this, CognitoService, Session);
                    break;
            }
            if (page_M != null)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Navigation.PopToRootAsync();
                    await Navigation.PushAsync(page_M.Page, true);
                });
            }
        }
    }
}
