﻿using mph_restaurant.Interface;
using mph_restaurant.Model.Util;
using mph_restaurant.Util;
using mph_restaurant.Util.Enum;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace mph_restaurant.Navigator
{
    public class Navigator_Public : INavigator<Event_Public>
    {
        public ICognitoService CognitoService { get; set; }
        public INavigation Navigation { get; set; }
        public ISession Session { get; set; }
        public Page Page { get; set; }
        
        public virtual async Task OnAlertEvent(Event_Public event_Public)
        {
            string title = "Unknown";
            string message = "Unknown";
            string cancel = "Ok";

            switch (event_Public)
            {
                #region Login
                case Event_Public.AllFieldsAreRequiered:
                    title = Strings_Public.Login_Fail;
                    message = Strings_Public.AllFieldsAreRequiered;
                    cancel = "Ir a validar";
                    break;
                case Event_Public.PasswordDontMatch:
                    title = Strings_Public.Login_Fail;
                    message = Strings_Public.PasswordDontMatch;
                    cancel = "Ir a validar";
                    break;
                case Event_Public.UserNotConfirmed:
                    title = Strings_Public.Login_Fail;
                    message = Strings_Public.UserNotConfirmed;
                    cancel = "Ir a validar";
                    break;
                case Event_Public.UserNotFound:
                    title = Strings_Public.Login_Fail;
                    message = Strings_Public.UserNotFound;
                    break;
                case Event_Public.NewPasswordRequired:
                    title = Strings_Public.Login_Fail;
                    message = Strings_Public.NewPasswordRequired;
                    break;
                case Event_Public.PasswordResetRequired:
                    title = Strings_Public.Login_Fail;
                    message = Strings_Public.PasswordResetRequired;
                    break;
                case Event_Public.NotAuthorized:
                    title = Strings_Public.Login_Fail;
                    message = Strings_Public.NotAuthorized;
                    break;
                #endregion
                #region SignUp
                case Event_Public.Registered:
                    title = Strings_Public.SignUp_Ok;
                    message = Strings_Public.Registered;
                    cancel = "Ir a validar";
                    break;
                case Event_Public.UsernameExists:
                    title = Strings_Public.SignUp_Fail;
                    message = Strings_Public.UsernameExists;
                    break;
                case Event_Public.InvalidPassword:
                    title = Strings_Public.SignUp_Fail;
                    message = Strings_Public.InvalidPassword;
                    break;
                case Event_Public.InvalidParameter:
                    title = Strings_Public.SignUp_Fail;
                    message = Strings_Public.InvalidParameter;
                    break;
                #endregion
                #region ValidateAccount
                case Event_Public.Verified:
                    title = Strings_Public.ValidateAccount_Ok;
                    message = Strings_Public.Verified;
                    cancel = "Ir a iniciar sesión";
                    break;
                case Event_Public.CodeMismatch:
                    title = Strings_Public.ValidateAccount_Fail;
                    message = Strings_Public.CodeMismatch;
                    break;
                #endregion
                case Event_Public.Unknown:
                    title = Strings_Public.Unknown_T;
                    message = Strings_Public.Unknown_M;
                    break;
            }
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Page.DisplayAlert(title, message, cancel);

                switch (cancel)
                {
                    case "Ir a validar":
                        await OnNavigationEvent(Event_Public.ToValidateAccount);
                        break;
                    case "Ir a iniciar sesión":
                        await OnNavigationEvent(Event_Public.ToLogin);
                        break;
                }
            });
        }

        public virtual async Task OnNavigationEvent(Event_Public event_Public)
        {
            Page_M page_M = null;

            switch (event_Public)
            {
                case Event_Public.ToHome:
                    (Application.Current as App).InitializeMainPage();
                    break;
                case Event_Public.ToLogin:
                    await Navigation.PopToRootAsync();
                    break;
                case Event_Public.ToSignUp:
                    page_M = Page_M.CreatePage_Public(PageId_Public.SignUp, this, CognitoService, Session);
                    break;
                case Event_Public.ToValidateAccount:
                    page_M = Page_M.CreatePage_Public(PageId_Public.ValidateAccount, this, CognitoService, Session);
                    break;
            }
            if (page_M != null)
            {
                await Navigation.PopAsync();
                await Navigation.PushAsync(page_M.Page, true);
            }
        }
    }
}
